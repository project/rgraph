<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/**
 * Developing a new RGraph 
 * First step of the form
 */
function rgraph_browser_develop_choose($form, &$form_state) {
	$form['#prefix'] = '<div id="rgraph-browser-develop-form-wrapper">';
	$form['#suffix'] = '</div>';
	$form['#tree'] = TRUE; 
	$form['#attributes']['enctype'] = "multipart/form-data";
	$step = empty($form_state['storage']['step']) ? 1 : $form_state['storage']['step'];
	$form_state['storage']['step'] = $step;
	switch ($step) {
    case 1:
      $form['step1'] = array(
        '#type' => 'fieldset',
        '#title' => t('Choose the type of RGraph'),
      );
	$types = _rgraph_type_list();
	$path = base_path() . drupal_get_path('module', 'rgraph');	
	foreach ($types as $type) {
		
		if($type->status == 1)
		{
			$attributes = '';
			$variables = array(
				'path' => $path . '/libraries/' . $type->name . '/screenshot.png',
				'title' => $type->info['name'],
				'alt' => $type->name,
				'getsize' => TRUE,
			);
			$output = '<img src=' . $variables['path'] . ' >';	
			$options[$type->name] = array(
				'title' => t('<b>' . check_plain($type->info['name']) . '</b>'),
				'pic' => t($output),
				'desc' => t(check_plain($type->info['description'])),
			);
		}
	}
	$header = array(
	  'title' => t(''),
	  'pic' => t(''),
	  'desc' => t(''),
	);
	$library_default = variable_get('library_default', 'bar');
	$form['step1']['rows'] = array(
	  '#type' => 'tableselect',
	  '#header' => $header,
	  '#options' => $options,
	  '#multiple' => FALSE,
	  '#empty' => t('No types are currently supported by the administratior'),
	  '#default_value' => empty($form_state['values']['step1']['rows']) ? $library_default : $form_state['values']['step1']['rows'],
	  '#required' => TRUE,
	);
      
		break;

    case 2:
      $form['step2'] = array(
        '#type' => 'fieldset',
        '#title' => t('Upload your data'),
      );
	  $validators = array(
	   'file_validate_extensions' => array(rgraph_variable_get('file_extensions')),
	   'file_validate_size' => array(parse_size(rgraph_variable_get('max_filesize'))),
	);


		// A blank set of allowed file extensions means no need to validate.
	if (!$validators['file_validate_extensions'][0]) {
		unset($validators['file_validate_extensions']);
	}
	  // Use the PHP limit for filesize if no variable was set.
	if (!$validators['file_validate_size']) {
		$validators['file_validate_size'] = file_upload_max_size();
	}

	if ($validators['file_validate_size'][0] == 0) {
		unset($validators['file_validate_size']);
	}

	$form['#validators'] = $validators;
	  $form['step2']['upload'] = array(
		'#type' => 'file',
		'#title' => t('Upload a new file'),
		'#description' => theme('file_upload_help', array('description' => '', 'upload_validators' => $validators)),
		'#upload_validators' => $validators,
	);
     
      break;
    case 3:
      $form['step3'] = array(
        '#type' => 'fieldset',
        '#title' => t('Format your RGraph'),
      );
	 
      break;
  }
  if ($step == 3) {
		$fieldName = 'step2';
	   
	  // If a file was uploaded, process it.
	  if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name'][$fieldName])) {

		// attempt to save the uploaded file
		$file = file_save_upload($fieldName);
		
		if ($file) {
		  drupal_set_message('The file attachment form has been submitted.');   
		}
		
		// set error if file was not uploaded
		if (!$file) {
		  form_set_error($fieldName, 'Error uploading file. Stage 1');
		  return;
		}
		   
		// set files to form_state, to process when form is submitted
		$form_state['values']['file'] = $file;
		   
	  }
	  else {
		// set error
		form_set_error($fieldName, 'Error uploading file. Stage 2');
		return;   
	  }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t("Submit"),
    );
  }
  if ($step < 3) {
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next step'),
      '#ajax' => array(
        'wrapper' => 'rgraph-browser-develop-form-wrapper',
        'callback' => 'rgraph_browser_develop_choose_callback',
      ),

    );
  }
  if ($step > 1) {
    $form['prev'] = array(
      '#type' => 'submit',
      '#value' => t("Previous step"),

      // Since all info will be discarded, don't validate on 'prev'.
      '#limit_validation_errors' => array(),
      // #submit is required to use #limit_validation_errors
      '#submit' => array('rgraph_browser_develop_choose_submit'),
      '#ajax' => array(
        'wrapper' => 'rgraph-browser-develop-form-wrapper',
        'callback' => 'rgraph_browser_develop_choose_callback',
      ),
    );
  }
  
  return $form;
  
 }
 
 
function rgraph_browser_develop_choose_callback($form, $form_state) {
  return $form;
}

function rgraph_browser_develop_choose_submit($form, &$form_state) {

  // Save away the current information.
  $current_step = 'step' . $form_state['storage']['step'];
  $form_state['storage']['values'][$current_step] = $form_state['values'][$current_step];

  // Increment or decrement the step as needed. Recover values if they exist.
  if ($form_state['triggering_element']['#value'] == t('Next step')) {
    $form_state['storage']['step']++;
    // If values had already been entered for this step, recover them from
    // $form_state['storage'] to pre-populate them.
    $step_name = 'step' . $form_state['storage']['step'];
    if (!empty($form_state['storage']['values'][$step_name])) {
      $form_state['values'][$step_name] = $form_state['storage']['values'][$step_name];
    }
  }
  if ($form_state['triggering_element']['#value'] == t('Previous step')) {
    $form_state['storage']['step']--;
    // Recover our values from $form_state['storage'] to pre-populate them.
    $step_name = 'step' . $form_state['storage']['step'];
    $form_state['values'][$step_name] = $form_state['storage']['values'][$step_name];
  }

  // If they're done, submit.
  if ($form_state['triggering_element']['#value'] == t('Submit')) {
    $value_message = t("Your information has been submitted: ");
    foreach ($form_state['storage']['values'] as $step => $values) {
      $value_message .= "$step: ";
      foreach ($values as $key => $value) {
       $value_message .= "$key=$value, ";
      }
    }
    drupal_set_message($value_message);
    $form_state['rebuild'] = FALSE;
    return;
  }

  // Otherwise, we still have work to do.

  $form_state['rebuild'] = TRUE;
}


function rgraph_browser_list(){
	$libraries = _rgraph_list_items();
	foreach ($libraries as $library) {
		
		if($type->status == 1)
		{
			$attributes = '';
			$variables = array(
				'path' => $path . '/libraries/' . $type->name . '/screenshot.png',
				'title' => $type->info['name'],
				'alt' => $type->name,
				'getsize' => TRUE,
			);
			$output = '<img src=' . $variables['path'] . ' >';	
			$options[$type->name] = array(
				'title' => t('<b>' . check_plain($type->info['name']) . '</b>'),
				'pic' => t($output),
				'desc' => t(check_plain($type->info['description'])),
			);
		}
	}
	$header = array(
	  'title' => t(''),
	  'pic' => t(''),
	  'desc' => t(''),
	);
}