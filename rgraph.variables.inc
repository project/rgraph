<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/* ***************************************** */
/* CONSTANTS                                 */
/* ***************************************** */

/**
 * @file Contains the variables used by rgraph and their defaults.
 */

/**
 * Define constants.
 */
define('RGRAPH_VARIABLE_NAMESPACE', 'rgraph__');

/**
 *  
 */
function rgraph_variable_get($name, $default = NULL) {
  // Allow for an override of the default.
  // Useful when a variable is required (like $path), but namespacing still desired.
  if (!isset($default)) {
    $default = rgraph_variable_default($name);
  }
  // Namespace all variables
  $variable_name = RGRAPH_VARIABLE_NAMESPACE . $name;
  return variable_get($variable_name, $default);
}

/**
 *  
 */
function rgraph_variable_set($name, $value) {
  $variable_name = RGRAPH_VARIABLE_NAMESPACE . $name;
  return variable_set($variable_name, $value);
}

/**
 *  
 */
function rgraph_variable_del($name) {
  $variable_name = RGRAPH_VARIABLE_NAMESPACE . $name;
  variable_del($variable_name);
}

/**
 * Returns the final machine name of a RGraph namespaced variable.
 */
function rgraph_variable_name($name) {
  return RGRAPH_VARIABLE_NAMESPACE . $name;
}

/**
 *  
 */
function rgraph_variable_default($name = NULL) {
  static $defaults;

  if (!isset($defaults)) {
    $defaults = array(
      'wysiwyg_title' => 'RGraph browser',
      'wysiwyg_icon_title' => 'Add RGraph',

      'field_select_rgraph_text' => 'Select rgraph',

      // Name of the theme to use in rgraph popup dialogs
      'dialog_get_theme_name' => 'seven',
      'file_extensions' => '.csv',
      'max_filesize' => '',
      'debug' => FALSE,

      'file_list_size' => 10,

      // Browser defaults in rgraph.browser.inc.
      'browser_viewtype_default' => 'thumbnails',
      'browser_pager_limit' => 10,
      'browser_library_empty_message' => 'There are currently no RGraphs in this library to select.',

      'import_batch_size' => 20,

    );
  }

  if (!isset($name)) {
    return $defaults;
  }

  if (isset($defaults[$name])) {
    return $defaults[$name];
  }
}
