<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/**
 * Filter callback for rgraph markup filter.
 *
 */
function rgraph_filter($text) {
  $text = ' ' . $text . ' ';
  $text = preg_replace_callback("/\[\[.*?]]/s",'_rgraph_markup', $text);

  return $text;
}

function _rgraph_markup($match, $wysiwyg = FALSE) {
  $match = str_replace("[[","",$match);
  $match = str_replace("]]","",$match);
  $tag = $match[0];

  try {
    if (!is_string($tag)) {
      throw new Exception('Unable to find matching tag');
    }
    $rgraph = drupal_json_decode($tag);
    if (!isset($rgraph['fid'])) {
      throw new Exception('No file Id');
    }
    if (!isset($rgraph['view_mode'])) {
     
      $rgraph['view_mode'] = rgraph_variable_get('wysiwyg_default_view_mode');
    }
    $rgraph_obj = rgraph_load($rgraph['fid']);
    if (!$rgraph_obj) {
     throw new Exception('Could not load rgraph object');
    }
    $settings = is_array($rgraph['attributes']) ? $rgraph['attributes'] : array();
    
    if (isset($settings['style'])) {
      if (preg_match('@width: (.+?)px; height: (.+?)px;@i', $settings['style'], $matches)) {
        $settings['width'] = $matches[1];
        $settings['height'] = $matches[2];
      }
    }
    if ($wysiwyg) {
      $settings['wysiwyg'] = $wysiwyg;
    }
  }
  catch (Exception $e) {
    watchdog('rgraph', 'Unable to render rgraph from %tag. Error: %error', array('%tag' => $tag, '%error' => $e->getMessage()));
    return '';
  }

  return drupal_render(rgraph_get_file_without_label($rgraph_obj, $rgraph['view_mode'], $settings));
}

function rgraph_pre_render_text_format($element) {
  
  if (!isset($element['format'])) {
    return $element;
  }

  $format_field = &$element['format'];
  $field = &$element['value'];
  $settings = array(
    'field' => $field['#id'],
  );
  
  $tagmap = _rgraph_generate_tagMap($field['#value']);

  if (isset($tagmap)) {
    drupal_add_js(array('tagmap' => array_unique($tagmap)), 'setting');
  }
  return $element;
}


function _rgraph_generate_tagMap($text) {
  static $tagmap = array();
  preg_match_all("/\[\[.*?]]/s", $text, $matches, PREG_SET_ORDER);
  foreach($matches as $match) {
    if(empty($tagmap[$match[0]])) {
      if ($markup_for_rgraph = _rgraph_markup($match, TRUE)) {
        $tagmap[$match[0]] = $markup_for_rgraph;
      } else {
        $tagmap[$match[0]] = '<div><img src="/broken.jpg" height="50px" width="100px"/></div>';
      }
    }
    else {
      return array();
    }
  }
  return $tagmap;
}


function rgraph_format_form($form, $form_state, $rgraph) {
  // This will vary depending on the rgraph type.

  $form = array();
  $form['#rgraph'] = $rgraph;
  $instance_info = field_info_instance('rgraph', 'file', $rgraph->type);
  $entity_info = entity_get_info('rgraph');

  $options = array();
  foreach ($entity_info['view modes'] as $key => $title) {
    $format = $instance_info['display'][$key];
    //If the format is set to hidden, don't offer it
    if ($format['type'] == 'hidden') {
      continue;
    }
    $options[$key] = $title['label'];
    $formats[$key] = drupal_render(rgraph_get_file_without_label($rgraph, $key, array('wysiwyg' => TRUE)));
  }

  if (!count($formats)) {
    throw new Exception('Unable to continue, no available formats for displaying rgraph.');
    return;
  }

  $default_view_mode = rgraph_variable_get('wysiwyg_default_view_mode');
  if (!isset($formats[$default_view_mode])) {
    $default_view_mode = key($formats);
  }

  // Add JS and settings array of formats.
  $settings = array();
  $settings['rgraph'] = array('formatFormFormats' => $formats);
  drupal_add_js($settings, 'setting');

  drupal_add_library('rgraph', 'rgraph_base');
  drupal_add_library('system', 'form');

  $path = drupal_get_path('module', 'rgraph');
  $form['#attached']['js'][] = $path . '/javascript/rgraph-format-form.js';
  $form['#attached']['css'][] = $path . '/css/rgraph-format-form.css';

  $form['heading'] = array(
    '#type' => 'markup',
    '#prefix' => '<h1 class="title">',
    '#suffix' => '</h1>',
    '#markup' => t('Embedding %filename', array('%filename' => $rgraph->filename)),
  );

  $preview = field_view_field('rgraph', $rgraph, 'file', 'rgraph_preview');
  $preview['#theme_wrappers'][] = 'rgraph_thumbnail';

  $form['preview'] = array(
    '#type' => 'markup',
    '#title' => basename($rgraph->uri),
    '#markup' => drupal_render($preview),
  );

  $form['format_group'] = array(
    '#type' => 'fieldset',
    '#title' => 'format',
    '#prefix' => t('<div class="clearfix" id="format-group-wrapper"><span id="format-description">Adding in <span id="selected-format">%currently_selected</span> format.  </span>', array('%currently_selected' => $options[$default_view_mode])),
    '#suffix' => t('</div>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['format_group']['format'] = array(
    '#type' => 'radios',
    '#title' => t('Format'),
    '#options' => $options,
    '#default_value' => $default_view_mode,
  );

  // These will get passed on to WYSIWYG
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('options'),
    '#description' => t('Alternate text'),
  );

  drupal_alter('rgraph_format_form_prepare', $form, $form_state, $rgraph);

  if (!element_children($form['options'])) {
    $form['options']['#attributes'] = array('style' => 'display:none');
  }

  return $form;
}


function rgraph_get_file_without_label($rgraph, $view_mode, $settings = array()) {
  $instance = field_info_instance('rgraph', 'file', $rgraph->type);
  $format = $instance['display'][$view_mode];
  $format['label'] = 'hidden';
  $format['settings'] = array_merge($format['settings'], $settings);
  $rgraph->override = $settings;
  return field_view_field('rgraph', $rgraph, 'file', $format);
}
