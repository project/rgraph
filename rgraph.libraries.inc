<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/**
 *  @file
 *  Functions related to listing of RGraph Libraries
 */

/**
 * Gets the records of the RGraph list of libraries based on the files array.
 *
 * @param $files
 *   An array of files.
 */
function rgraph_get_libraries() {
	$result = db_query("SELECT filename, name, info, weight FROM {rgraph_libraries_list}");
	return $result;
}


function rgraph_get_libraries_database(&$files) {
  $result = db_query("SELECT filename, name, status, weight FROM {rgraph_libraries_list}");
  foreach ($result as $file) {
    if (isset($files[$file->name]) && is_object($files[$file->name])) {
      $file->uri = $file->filename;
      foreach ($file as $key => $value) {
        if (!isset($files[$file->name]->$key)) {
          $files[$file->name]->$key = $value;
        }
      }
    }
  }
}

/**
 * Updates the records in the RGraph list of libraries based on the files array.
 *
 * @param $files
 *   An array of files.
 */
function rgraph_update_libraries_database(&$files) {
  $result = db_query("SELECT * FROM {rgraph_libraries_list}");

  // Add all files that need to be deleted to a DatabaseCondition.
  $delete = db_or();
  foreach ($result as $file) {
    if (isset($files[$file->name]) && is_object($files[$file->name])) {
      // Keep the old filename from the database in case the file has moved.
      $old_filename = $file->filename;

      $updated_fields = array();

      // Handle info specially, compare the serialized value.
      $serialized_info = serialize($files[$file->name]->info);
      if ($serialized_info != $file->info) {
        $updated_fields['info'] = $serialized_info;
      }
      unset($file->info);

      // Scan remaining fields to find only the updated values.
      foreach ($file as $key => $value) {
        if (isset($files[$file->name]->$key) && $files[$file->name]->$key != $value) {
          $updated_fields[$key] = $files[$file->name]->$key;
        }
      }

      // Update the record.
      if (count($updated_fields)) {
        db_update('rgraph_libraries_list')
          ->fields($updated_fields)
          ->condition('filename', $old_filename)
          ->execute();
      }

      // Indicate that the file exists already.
      $files[$file->name]->exists = TRUE;
    }
    else {
      $delete->condition('filename', $file->filename);
    }
  }

  if (count($delete) > 0) {
    db_delete('rgraph_libraries_list')
      ->condition($delete)
      ->execute();
  }

  $query = db_insert('rgraph_libraries_list')->fields(array('filename', 'name', 'info'));
  foreach($files as &$file) {
    if (isset($file->exists)) {
      unset($file->exists);
    }
    else {
      $query->values(array(
        'filename' => $file->uri,
        'name' => $file->name,
		'info' => serialize($file->info),
      ));
      $file->status = 0;
    }
  }
  $query->execute();
}

/**
 * Returns an array of information about active libraries.
 *
 * This function returns the information from the {rgraph_libraries_list} table corresponding
 * to the cached contents of the .info file for each active module or theme.
 *
 */
function rgraph_libraries_get_info() {
  $info = array();
  $result = db_query('SELECT name, info FROM {rgraph_libraries_list} WHERE status = 1');
  foreach ($result as $item) {
    $info[$item->name] = unserialize($item->info);
  }
  return $info;
}


/**
 * Rebuild RGraph Library Type Data
 *
 * @param $files
 *   An array of files.
 */

function _rgraph_rebuild_type_data() {
  $libraries = drupal_system_listing('/\.info$/', 'sites/all/modules/rgraph/libraries');
  
  $defaults = array(
    'regions' => array(
      'content' => 'Content',
      'legend' => 'Legend',
      'datatable' => 'Data Table',
      'header' => 'Header',
      'haxis' => 'Horizontal Axis',
      'vaxis' => 'Vertical Axis',
    ),
    'description' => '',
    'screenshot' => 'screenshot.png',
  );

  foreach ($libraries as $key => $library) {
    $libraries[$key]->filename = $library->uri;
    $libraries[$key]->info = drupal_parse_info_file($library->uri) + $defaults;

    // Give the stylesheets proper path information.
    $pathed_stylesheets = array();
    if (isset($libraries[$key]->info['stylesheets'])) {
      foreach ($libraries[$key]->info['stylesheets'] as $media => $stylesheets) {
        foreach ($stylesheets as $stylesheet) {
          $pathed_stylesheets[$media][$stylesheet] = dirname($libraries[$key]->uri) . '/' . $stylesheet;
        }
      }
    }
    $libraries[$key]->info['stylesheets'] = $pathed_stylesheets;

    // Give the scripts proper path information.
    $scripts = array();
    if (isset($libraries[$key]->info['scripts'])) {
      foreach ($libraries[$key]->info['scripts'] as $script) {
        $scripts[$script] = dirname($libraries[$key]->uri) . '/' . $script;
      }
    }
    $libraries[$key]->info['scripts'] = $scripts;
    // Give the screenshot proper path information.
    if (!empty($libraries[$key]->info['screenshot'])) {
      $libraries[$key]->info['screenshot'] = dirname($libraries[$key]->uri) . '/' . $libraries[$key]->info['screenshot'];
    }
  }

  return $libraries;
}


/**
 * Rebuild, save, and return data about all currently available libraries.
 *
 * @return
 *   Array of all available libraries and their data.
 */
function rgraph_rebuild_type_data() {
  $libraries = _rgraph_rebuild_type_data();
  ksort($libraries);
  rgraph_get_libraries_database($libraries, 'theme');
  rgraph_update_libraries_database($libraries, 'theme');
  return $libraries;
}


/**
 * Get RGraph list of libraries
 *
 * @param $files
 *   An array of files.
 */

function _rgraph_type_list() {
  $libraries = rgraph_rebuild_type_data();
 
  foreach ($libraries as $library_key => $library) {
    if (!empty($library->info['hidden'])) {
      unset($libraries[$library_key]);
    }
  }

 
  return $libraries;
}
