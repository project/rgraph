<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/**
 * @file
 * RGraph Browser page callback
 *
 */
function rgraph_browser($selected = NULL) {
	$output = array();
	return $output;
}


/**
 * Launching the  RGraph browser through WYSIWYG Editor
 */
function rgraph_include_browser_js() {
  static $included;
  if ($included) {
    return;
  }
  $included = TRUE;
  drupal_add_library('rgraph', 'rgraph_browser');

  $settings = array(
    'browserUrl' => url('rgraph/browser',
      array('query' => array('render' => 'rgraph-popup'))),
    'styleSelectorUrl' => url('rgraph/-rgraph_id-/format-form',
      array('query' => array('render' => 'rgraph-popup'))),
  );

  drupal_add_js(array('rgraph' => $settings), 'setting');
}
