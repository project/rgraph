<?php

/**
 *  @file
 *  Define the WYSIWYG browser plugin.
 */
/**
 * Here is the odd thing - this function must be named:
 * <module_name>_<plugin_name>_plugin
 */
/**
 * Implementation of WYSIWYG's hook_INCLUDE_plugin().
 */
function rgraph_rgraph_plugin() {
   // Plugin definition
   module_load_include('inc','rgraph','rgraph.browser');
   rgraph_include_browser_js();
   
  $plugins['rgraph'] = array(
    'title' => t('RGraph browser'),
    'vendor url' => 'http://drupal.org/project/rgraph',
	'css path' => drupal_get_path('module','rgraph').'/css/',
    'icon file' => 'wysiwyg-rgraph.png',
	'icon path' => drupal_get_path('module','rgraph').'/images',
    'icon title' => t('Add RGraph'),
	'js path' => drupal_get_path('module','rgraph').'/javascript/',
	'js file' => 'rgraph.wysiwyg.js',
	'module' => 'rgraph',
    'settings' => array(),
   );

   return $plugins;
}
