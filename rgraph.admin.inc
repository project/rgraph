<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/**
 * @file
 * This file contains the admin functions for the RGraph module.
 */


/**
 * Display all rgraph types.
 */
function rgraph_admin_type_list() {
// Get current list of libraries.
  $libraries = _rgraph_type_list();

  $library_default = variable_get('library_default', 'bar');
  $library_groups  = array();

  foreach ($libraries as &$library) {
    $admin_library_options[$library->name] = $library->info['name'];
    $library->is_default = ($library->name == $library_default);

    // Identify library screenshot.
    $library->screenshot = NULL;
    if (isset($libraries[$library->name]->base_libraries)) {
      $library_keys = array_keys($libraries[$library->name]->base_libraries);
      $library_keys[] = $library->name;
    }
    else {
      $library_keys = array($library->name);
    }
    // Look for a screenshot in the current library or in its closest ancestor.
    foreach (array_reverse($library_keys) as $library_key) {
      if (isset($libraries[$library_key]) && file_exists($libraries[$library_key]->info['screenshot'])) {
        $library->screenshot = array(
          'path' => $libraries[$library_key]->info['screenshot'],
          'alt' => t('Screenshot for !library library', array('!library' => $library->info['name'])),
          'title' => t('Screenshot for !library library', array('!library' => $library->info['name'])),
          'attributes' => array('class' => array('screenshot')),
          'getsize' => FALSE,
        );
        break;
      }
    }

    if (empty($library->status)) {
      $library->incompatible_core = !isset($library->info['core']) || ($library->info['core'] != DRUPAL_CORE_COMPATIBILITY) || (!isset($library->info['regions']['content']));
    }
    $query['token'] = drupal_get_token('rgraph-library-operation-link');
    $library->operations = array();
	$query['library'] = $library->name;
    if (!empty($library->status)) {
      // Create the operations links.
      
      if (!empty($library->status)) {
        $library->operations[] = array(
          'title' => t('Settings'),
          'href' => 'admin/structure/rgraph/settings/' . $library->name,
		  'attributes' => array('title' => t('Settings for !library library', array('!library' => $library->info['name']))),
        );
      }
	  }
      if (!empty($library->status)) {
        if (!$library->is_default) {
          $library->operations[] = array(
            'title' => t('Disable'),
            'href' => 'admin/structure/rgraph/disable',
            'query' => $query,
            'attributes' => array('title' => t('Disable !library library', array('!library' => $library->info['name']))),
          );
          $library->operations[] = array(
            'title' => t('Set default'),
            'href' => 'admin/structure/rgraph/default',
            'query' => $query,
            'attributes' => array('title' => t('Set !library as default library', array('!library' => $library->info['name']))),
          );
        }
      }
	  
      else {
        $library->operations[] = array(
          'title' => t('Enable'),
          'href' => 'admin/structure/rgraph/enable',
          'query' => $query,
          'attributes' => array('title' => t('Enable !library library', array('!library' => $library->info['name']))),
        );
        $library->operations[] = array(
          'title' => t('Enable and set default'),
          'href' => 'admin/structure/rgraph/default',
          'query' => $query,
          'attributes' => array('title' => t('Enable !library as default library', array('!library' => $library->info['name']))),
        );
      }
    

    // Add notes to default and administration library.
    $library->notes = array();
    $library->classes = array();
    if ($library->is_default) {
      $library->classes[] = 'library-default';
      $library->notes[] = t('default library');
    }

    // Sort enabled and disabled libraries into their own groups.
    $library_groups[$library->status ? 'enabled' : 'disabled'][] = $library;
  }

	$library_group_titles = array(
		'enabled' => format_plural(count($library_groups['enabled']), 'Enabled library', 'Enabled libraries'),
  );
	if (!empty($library_groups['disabled'])) {
		$library_group_titles['disabled'] = format_plural(count($library_groups['disabled']), 'Disabled library', 'Disabled libraries');
	}
  return theme('system_themes_page', array('theme_groups' => $library_groups, 'theme_group_titles' => $library_group_titles));
  
}

function library_enable($library) {
	foreach ($library as $key) {
    db_update('rgraph_libraries_list')
      ->fields(array('status' => 1))
      ->condition('name', $key)
      ->execute();
  }
  rgraph_rebuild_type_data();
}

function library_disable($library) {
	foreach ($library as $key) {
    db_update('rgraph_libraries_list')
      ->fields(array('status' => 0))
      ->condition('name', $key)
      ->execute();
  }
  rgraph_rebuild_type_data();
}

/**
 * Menu callback; Enables a library.
 */
function rgraph_library_enable() {
  if (isset($_REQUEST['library']) && isset($_REQUEST['token']) && drupal_valid_token($_REQUEST['token'], 'rgraph-library-operation-link')) {
    $library = $_REQUEST['library'];
    // Get current list of libraries.
    $libraries = _rgraph_type_list();

    // Check if the specified library is one recognized by the system.
    if (!empty($libraries[$library])) {
      library_enable(array($library));
      drupal_set_message(t('The %library library has been enabled.', array('%library' => $libraries[$library]->info['name'])));
    }
    else {
      drupal_set_message(t('The %library library was not found.', array('%library' => $library)), 'error');
    }
    drupal_goto('admin/structure/rgraph');
  }
  return drupal_access_denied();
}


/**
 * Menu callback; Disables a library.
 */
function rgraph_library_disable() {
  if (isset($_REQUEST['library']) && isset($_REQUEST['token']) && drupal_valid_token($_REQUEST['token'], 'rgraph-library-operation-link')) {
    $library = $_REQUEST['library'];
    // Get current list of libraries.
    $libraries = _rgraph_type_list();

    // Check if the specified library is one recognized by the system.
    if (!empty($libraries[$library])) {
      if ($library == variable_get('library_default', 'bar')) {
        // Don't disable the default library.
        drupal_set_message(t('%library is the default library and cannot be disabled.', array('%library' => $libraries[$library]->info['name'])), 'error');
      }
      else {
        library_disable(array($library));
        drupal_set_message(t('The %library library has been disabled.', array('%library' => $libraries[$library]->info['name'])));
      }
    }
    else {
      drupal_set_message(t('The %library library was not found.', array('%library' => $library)), 'error');
    }
    drupal_goto('admin/structure/rgraph');
  }
  return drupal_access_denied();
}


/**
 * Menu callback; Set the default library.
 */
function rgraph_library_default() {
  if (isset($_REQUEST['library']) && isset($_REQUEST['token']) && drupal_valid_token($_REQUEST['token'], 'rgraph-library-operation-link')) {
    $library = $_REQUEST['library'];
    // Get current list of libraries.
    $libraries = _rgraph_type_list();

    // Check if the specified library is one recognized by the rgraph.
    if (!empty($libraries[$library])) {
      // Enable the library if it is currently disabled.
      if (empty($libraries[$library]->status)) {
       library_enable(array($library));
      }
      // Set the default library.
      variable_set('library_default', $library);
      // The status message depends on whether an admin library is currently in use:
      // a value of 0 means the admin library is set to be the default library.
      $admin_library = variable_get('admin_library', 0);
      if ($admin_library != 0 && $admin_library != $library) {
        drupal_set_message(t('Please note that the administration library is still set to the %admin_library library; consequently, the library on this page remains unchanged. All non-administrative sections of the site, however, will show the selected %selected_library library by default.', array(
          '%admin_library' => $libraries[$admin_library]->info['name'],
          '%selected_library' => $libraries[$library]->info['name'],
        )));
      }
      else {
        drupal_set_message(t('%library is now the default library.', array('%library' => $libraries[$library]->info['name'])));
      }
    }
    else {
      drupal_set_message(t('The %library library was not found.', array('%library' => $library)), 'error');
    }
    drupal_goto('admin/structure/rgraph');
  }
  return drupal_access_denied();
}


/**
 *  The administration form for managing features for rgraph types.
 */
function rgraph_admin_type_manage_form($form, &$form_state, $rgraph_type) {
   $form = array();
	  $form['rgraph_type'] = array(
		'#type' => 'value',
		'#value' => $rgraph_type->name,
	  );
	  $library = unserialize($rgraph_type->info);
	  $form['type'] = array(
		'#type' => 'fieldset',
		'#title' => t($library['name']),
	  );
	  
  
  $allfeatures = array(
	'charttitle' => t('Chart Title'),
	'haxislable' => t('Horizontal Axis Label'),
	'vaxistable' => t('Vertical Axis Label'),
	'legend' => t('Legend Table'),
	'datatable' => t('Data Table'),
	'datavalues' => t('Data Values'),
);
	
  $features =  array(
	  'zoom' => t('Zoom'),
	  'annotable' => t('Annotable'),
	  'tooltips' => t('Tool Tips'),
  );
  $form['allchecksets'] = array(
		'#type' => 'fieldset',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#title' => t('Features'),
		
	  );
	  $form['allchecksets']['features'] = array(
    '#type' => 'checkboxes',
    '#options' => $allfeatures,
	'#description' => t('Please check out all the options you wish the user to input in this graph'),
  );
  $form['checksets'] = array(
		'#type' => 'fieldset',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#title' => t('Allowed Extra Features'),
		
	  );
  
  $form['checksets']['features'] = array(
    '#type' => 'checkboxes',
    '#options' => $features,
  );
	
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
    '#weight' => 100,
  );
  return $form;
}

function rgraph_admin_type_manage_form_submit($form, &$form_state) {
  $rgraph_type = rgraph_type_load($form_state['values']['rgraph_type']);
  if ($form_state['values']['match_type']) {
    $rgraph_type->type_callback_args['match_type'] = $form_state['values']['match_type'];
  }
  else {
    $rgraph_type->type_callback_args['match_type'] = $form_state['values']['match_type_other'];
  }
  $rgraph_type->type_callback_args['streams'] = array();
  foreach ($form_state['values']['streams'] as $stream) {
    if ($stream) {
      $rgraph_type->type_callback_args['streams'][] = $stream;
    }
  }
  $rgraph_type->type_callback_args['mimetypes'] = explode(' ', $form_state['values']['mimetypes']);
  $rgraph_type->type_callback_args['extensions'] = explode(' ', $form_state['values']['extensions']);

  rgraph_type_save($rgraph_type);
  drupal_set_message(t('The @label rgraph type has been saved.', array('@label' => $rgraph_type->label)));
}

