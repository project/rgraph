<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/**
 * Display the rgraph file browser.
 */
function theme_rgraph_file_browser($element) {
  // Add the CSS for our display.
  $output = '<div class="rgraph browser">' . $element . '</div>';

  return $output;
}

/**
 * Display a rgraph file list.
 */
function theme_rgraph_file_list($element) {
  // Add the CSS for our display.
  return '<div class="rgraph-file-list">' . theme('form_element', $element, $element['#children']) . '</div>';
}

/**
 * Display a browser pane.
 */
function theme_rgraph_browser_pane($form) {
  return;
  $output   = array();

  // render the drawers
  $output[] = '<div' . drupal_attributes($form['#attributes']) . '>';

  // render the drawer list
  $output[] = '  <div class="browser drawers">';
  $output[] = drupal_render_form(null, $form['drawers']);
  $output[] = ' </div>';

  // render the drawer displays
  $output[] = drupal_render_form(null, $form);
  $output[] = '</div>';

  return implode("\n", $output);
}


function theme_rgraph_browser_content_frame($variables) {
  // Pull out all the variables into a usable form
  extract($variables);
  // Did we get any files back?
  if (! count($files)) {
    // @TODO display no files found
  }

  $html = array();

  // On the first invocation, load javascript and build the browser frame
   if ($invoke) {


  }
   // Render the results limiter
  $html[] = theme('rgraph_browser_control_result_limit', array('parameters' => $parameters));
  // Render the actual content
  $html[] = drupal_render(drupal_get_form('rgraph_file_listing_form', $files, $parameters));

  // Make sure to close the wrapping div
  if ($invoke) {
    $html[] = '</div>';
  }
  return implode("\n", $html);
}

function theme_rgraph_browser_thumbnails($variables) {
  $files = $variables['files'];
  $style_name = $variables['style_name'];
  $thumbnails = array();
  foreach ($files as $file) {
    $thumbnails[] = theme('rgraph_admin_thumbnail', array('file' => $file, 'style_name' => $style_name));
  }
  return theme('item_list', array('items' => $thumbnails, 'attributes' => array('class' => 'rgraph_content_navigator results')));
}


function theme_rgraph_admin_thumbnail($variables) {
  $path = drupal_get_path('module', 'rgraph');
  $file = $variables['file'];
  $style_name = $variables['style_name'];
  if (isset($file)) {
    $file_url = file_create_url($file->uri);
  }
  else {
    return '';
  }
  $output = '';
  if (module_exists('styles')) {
    $thumbnail = theme('styles',
      array(
        'field_type' => 'file',
        'style_name' => $style_name,
        'uri' => $file->uri,
        'description' => t('Thumbnail for !filename.', array('!filename' => $file->filename)),
        'object' => $variables['file'],
    ));
  }
  else {
    // Display a thumbnail for images.
    if (strstr($file->filemime, 'image')) {
      $thumbnail = theme('image_style',
        array(
          'style_name' => 'thumbnail',
          'path' => $file->uri,
          'alt' => t('Thumbnail for !filename.', array('!filename' => $file->filename)),
        )
      );
    }
    // Display the 'unknown' icon for other file types.
    else {
      $thumbnail = theme('image',
        array(
          'path' => $path . '/images/file-unknown.png',
          'alt' => t('Thumbnail for !filename.', array('!filename' => $file->filename)),
          'attributes' => array('class' => 'file-unknown'),
        ));
    }
  }
  $output .= l($thumbnail,
    $file_url,
    array(
      'html' => TRUE,
      'attributes' => array('class' => 'rgraph-thumbnail'),
    ));
  return $output;
}


/**
 * Theme operations for a thumbnail.
 */
function theme_rgraph_admin_thumbnail_operations($variables) {
  $destination = drupal_get_destination();
  $file = $variables['file'];
  $output = l(t('edit'), 'admin/content/rgraph/' . $file->fid . '/edit', array('query' => $destination));
  return $output;
}

/**
 * Add messages to the page.
 */
function template_preprocess_rgraph_dialog_page(&$variables) {
  $variables['messages'] = theme('status_messages');
}

function theme_rgraph_browser_control_result_limit($variables) {
  // Pull out all the variables into a usable form
  extract($variables);

  if (! isset($limits)) {
    $limits = array(10, 30, 50);
  }
  
  $parameters['page'] = 0;
  // save the active limit
  $current_limit = $parameters['limit'];
  foreach ($limits as $limit) {
    if ($limit == $current_limit) {
      $class = 'active';
    }
    else {
      $class = '';
    }
    // set the value of this limit parameter to this limit value
    $parameters['limit'] = $limit;
    $per_display[] = l($limit, $limit, array('query' => $parameters, 'attributes' => array('class' => $class)));
  }

  return theme('item_list', array('items'  => $per_display, 'attributes' => array('class' => 'result_limit')));
}

function theme_rgraph_link($variables) {
  $file = $variables['file'];

  $url = 'rgraph/' . $file->fid;
  $icon = theme('file_icon', array('file' => $file));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = check_plain($file->filename);
  }
  else {
    $link_text = check_plain($file->description);
    $options['attributes']['title'] = check_plain($file->filename);
  }

  return '<span class="file">' . $icon . ' ' . l($link_text, $url, $options) . '</span>';
}

function theme_rgraph_thumbnail($variables) {
  $label = '';
  $element = $variables['element'];
  // Element should be a field renderable array... This is a little wonky - admitted.
  if (!empty($element['#show_names']) && $element['#object']->filename) {
    $label = '<label class="rgraph-filename">' . $element['#object']->filename . '</label>';
  }
  // I'm sure this is the wrong way to do this, but can't figure it out... :(
  return '<div class="rgraph-thumbnail">' . $element['#children'] . $label . '</div>';
}

/**
 * Theme a managed file element.
 */
function theme_rgraph_element($variables) {
  $element = $variables['element'];

  // This wrapper is required to apply JS behaviors and CSS styling.
  $output = '';
  $output .= '<div ' . drupal_attributes($element['#attributes']) . '>';
  $output .= drupal_render_children($element);
  $output .= '</div>';
  return $output;
}