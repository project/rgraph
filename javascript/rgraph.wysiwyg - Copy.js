
(function ($) {
// @todo Changing from copy from break.js
Drupal.wysiwyg.plugins['rgraph'] = {

  currentNode: null,

  /**
   * Return whether the passed node belongs to this plugin.
   */
  isNode: function(node) {
    //if (!$(node).is('body')) { alert($(node).html()); }
    return $(node).is('.rgraph-insert') || $(node).is('.rgraph-caption');
  },

  /**
   * Execute the button.
   *
   * Note that data is an object with three properties:
   *   format describes what the format is - we care about 'html', mostly.
   *   node is the DOM node of the item invoked.
   *   content is the HTML of the item invoked.
   *
   * Settings is an object that seems to only include path?  Hm...
   */
  invoke: function(data, settings, instanceId) {
    if (data.format == 'html') {
      // Prevent duplicating a video.
      if (this.isNode(data.node)) {
        //alert($(data.node).parent().get(0));
        // Even if the caption was clicked on, we want to find the image node.
        var imageNode = $('img', $(data.node).parent().get(0)).get(0);

        //alert(object2String(Drupal.wysiwyg.instances[instanceId]));
        this.currentNode = $(data.node).parent().get(0);

        var attributes = this._getPropertiesFromPlaceholder(imageNode);
        this.createBrowserWindow(settings, instanceId, attributes);
        return;
      }
      this.createBrowserWindow(settings, instanceId, this._getPropertiesFromPlaceholder(null));
    }
    else {
      // Prevent duplicating a video.
      // @todo data.content is the selection only; needs access to complete content.
      if (data.content.match(/<dme:image \/>/)) { // @todo Is this right?
        return;
      }
      var content = '<dme: image />';
    }
    if (typeof content != 'undefined') {
      Drupal.wysiwyg.instances[instanceId].insert(content);
    }
  },

  /**
   * Replace all <dme:image> tags with images.  Since the fckEditor tends to turn the tag into <dme:image></dme:image> we check for that too.
   */
  attach: function(content, settings, instanceId) {
    var tagRegex = /<dme:image\s*((?:\s+\w+=([-+]?[0-9.]+|(["']).+?\3))*)\s*>(.*?)<\/dme:image>/ig;
    var attributeRegex = /(\w+)=([-+]?[0-9.]+|(["'])([^;]+?)\3)/ig;
    var matches = tagRegex.exec(content);

    //alert(matches);
    while (matches != null) {
      var numberProperty = -1;
      var align = '';
      var count = 0;
      var caption = matches[4];
      //alert("Caption " + caption);
      var attrMatches = attributeRegex.exec(matches[1]);
      //alert(matches[0]);
      while (attrMatches != null && count < 10) {
        //alert(attrMatches[0] + ' ' + attrMatches[1]);
        switch (attrMatches[1]) {
          case 'number':
            if (attrMatches[4]) {
              numberProperty = attrMatches[4];
            }
            else {
              numberProperty = attrMatches[2];
            }
            break;
          case 'align':
            if (attrMatches[4]) {
              align = attrMatches[4];
            }
            else {
              align = attrMatches[2];
            }
            break;
          default:
        }
        attrMatches = attributeRegex.exec(matches[1]);
        count = count + 1;
      }

      //alert(this._getPlaceholder(settings, numberProperty, align, caption));
      content = content.replace(matches[0], this._getPlaceholder(settings, numberProperty, align, caption));
      matches = tagRegex.exec(content);
    }
    return content;
  },

  /**
   * Replace images with <dme:image> tags in content upon detaching editor.
   */
  detach: function(content, settings, instanceId) {
    var $content = $('<div>' + content + '</div>'); // No .outerHTML() in jQuery :(
    // document.createComment() required or IE will strip the comment.
    $('div.rgraph-insert', $content).each(
      function (i) {
        var attributes = Drupal.wysiwyg.plugins['rgraph']._getPropertiesFromPlaceholder($('img', this).get(0));
        //alert(object2String(attributes));
        $(this).replaceWith('<dme:image number="' + attributes.number + '" align="' + attributes.align + '">' + attributes.caption + '</dme:image>');
      }
    );
    return $content.html();
  },

  /**
   * Helper function to return a HTML placeholder.
   */
  _getPlaceholder: function (settings, numberProperty, align, caption) {
    var properties = '';
    if (numberProperty) {
      properties = 'number=' + numberProperty + ';';
    }
    if (align) {
      properties = properties + 'align=' + align + ';';
    }
    var path = settings.path + '/images/Video.png';

    //alert('edit-field-image-' + (numberProperty - 1) + '-wrapper div.imagefield-preview img');
    path = $('#edit-field-image-' + (numberProperty - 1) + '-wrapper div.imagefield-preview img').attr('src');
    path = path.replace(/imagefield_thumbs/, 'imagecache/' + Drupal.settings.dme_image.namespace);
    /*if (Drupal.settings.dme_image.placeholder_path) {
      alert("path is " + Drupal.settings.dme_image.placeholder_path);
      path = Drupal.settings.dme_image.placeholder_path
    }*/

    var style = '';
    if (align == 'left') {
      style = 'style = "float: left; margin: 5px 5px 5px 0px;"';
    }
    if (align == 'right') {
      style = 'style = "float: right; margin: 5px 0px 5px 5px;"';
    }
    if (align == 'center') {
      style = 'style = "margin-left: auto; margin-right: auto; margin-top: 5px; margin-bottom: 5px;"';
    }

    //return '<img src="' + path + '" alt="' + properties + '" title="' + caption + '" class="rgraph-insert" style="float: ' + align + '" />';
    var returnString = '<div class="rgraph rgraph-insert rgraph-' + align + '" ' + style + '>' +
      '<img src="' + path + '" alt="' + properties + '" title="' + caption + '" class="rgraph-insert" />';
    if (caption != '') {
      returnString = returnString + '<div class="rgraph-caption">' + caption + '</div>';
    }
    return returnString + '</div>';
  },

  /**
   * This function takes the current settings and instance, along with an attributes object, and presents the user
   * with a wysiwyg pop-up form to make selectons on as to the display of the image.
   *
   * If the user has selected an existing image, the attributes object will contain pre-existing values that we will
   * feed into the form.
   */
  createBrowserWindow: function (settings, instanceId, attributes) {
    var buttonText = 'Insert Image';
    if (attributes.align != '') {
      buttonText = 'Update Image';
    }
    $('body').append('<div id="rgraph-div">' +
                     '<h1>Enter Image Data</h1>' +
                     '<div id="closeButton"></div>' +
                     '<br />' +
                     '<form id="rgraph-form">' +
                     '  <input type="hidden" name="instanceid" id="hidden-instanceid" value="' + instanceId + '"/>' +
                     '  <input type="hidden" name="path" id="hidden-path" value="' + settings.path + '" />' +
                     '  <label for="number">Number:</label><input type="textfield" value="' + attributes.number +'" name="number" id="edit-number"/>' +
                     '  <label for="align">Alignment:</label><select name="align" id="edit-align">' +
                     '    <option value="left" ' + (attributes.align == 'left' ? 'selected' : '') + '>Left</option>' +
                     '    <option value="center" ' + (attributes.align == 'center' ? 'selected' : '') + '>Center</option>' +
                     '    <option value="right" ' + (attributes.align == 'right' ? 'selected' : '') + '>Right</option>' +
                     '</select>' +
                     '  <br /><label for="caption">Caption:</label><textarea rows="2" cols="30" name="caption" id="edit-caption">' + attributes.caption + '</textarea>' +
                     '  <br /><input type="submit" value="' + buttonText + '"/>' +
                     '  <input type="button" value="Cancel" id="rgraph-form-cancel" />' +
                     '</form></div>');
    $('form#rgraph-form').submit(function () {
      var id = $('form#rgraph-form input#hidden-instanceid').val();
      var number = $('form#rgraph-form input#edit-number').val();
      var align = $('form#rgraph-form #edit-align').val();
      var path = $('form#rgraph-form input#hidden-path').val();
      var caption = $('form#rgraph-form #edit-caption').val();
      //alert(id);
      var content = Drupal.wysiwyg.plugins['rgraph']._getPlaceholder({'path' : path}, number, align, caption);

      if (Drupal.wysiwyg.plugins['rgraph'].currentNode != null) {
        $(Drupal.wysiwyg.plugins['rgraph'].currentNode).remove();
      }

      Drupal.wysiwyg.instances[id].insert(content);
      $('.rgraph-left').css('float', 'left').css('margin', '5px 5px 5px 0px');
      $('.rgraph-right').css('float', 'right').css('margin', '5px 0px 5px 5px');
      $('div#rgraph-div').remove();
      return false;
    });
    $('form#rgraph-form input#rgraph-form-cancel').click(function () {
      $('div#rgraph-div').remove();
      return false;
    });
    $('div#closeButton').click(function () {
      $('div#rgraph-div').remove();
    });
    var closeButton = {
      'background-image':'url(' + settings.path + '/images/sprites.png)',
      'background-position':'-16px -651px',
      'background-repeat':'no-repeat',
      'cursor':'pointer',
      'height':'20px',
      'margin-right':'10px',
      'margin-top':'5px',
      'position':'absolute',
      'right':'0',
      'top':'0',
      'width':'20px'
    };
    $('div#closeButton').css(closeButton);
    var cssProps = {
      'height' : '200px',
      'width' : '300px',
      'position' : 'fixed',
      'left' : ((window.screen.width - 300) / 2) + 'px',
      'top' : ((window.screen.height - 200) / 2) + 'px',
      'zorder' : 1000,
      'background-color' : '#E3E3C7',
      'border' : '2px solid black' };
    $('#rgraph-div').css(cssProps);
  },

  /**
   * This function takes a placeholder object that has been placed in the text, and retreives from it
   * the various attributes.  If imgPlaceholder is null, then we return default values in the attributes.
   */
  _getPropertiesFromPlaceholder : function (imgPlaceholder) {
    var number = 1;
    var align = '';
    var caption = '';

    if (imgPlaceholder && imgPlaceholder != null) {
      var properties = $(imgPlaceholder).attr('alt');
      //alert(properties);
      var matches = properties.match(/number=(\d+);/);
      if (matches != -1) {
        number = matches[1];
      }
      matches = properties.match(/align=(\w+);/);
      if (matches != -1) {
        align = matches[1];
      }

      caption = $(imgPlaceholder).attr('title');
    }

    return {
      'number'  : number,
      'align'   : align,
      'caption' : caption
    };
  }
};
})(jQuery);