var selectedRGraph = [];
(function ($) {

Drupal.wysiwyg.plugins['rgraph'] = {
//-------------------------------------------------------------------------------------------------------------------------------------
	isNode: function(node) {
		return ($(node).is('img.rgraph-rgraph'));
	},
//-------------------------------------------------------------------------------------------------------------------------------------  
	invoke: function(data, settings, instanceId) {
		if (data.format == 'html') {
			this.launchBrowserWindow(function (rgraphCharts) {
				this.RGraphBrowserOnSelect(rgraphCharts, instanceId);
			});
		}
	},
//-------------------------------------------------------------------------------------------------------------------------------------
	RGraphBrowserOnSelect: function (rgraphCharts, instanceId) {
      var rgraphChart = rgraphCharts[0];
      var options = {};
      this.rgraphStyleSelector(rgraphChart, function (formattedRGraph) {
			this.insertrgraphChart(rgraphChart, formattedRGraph.type, formattedRGraph.html, formattedRGraph.options, Drupal.wysiwyg.instances[instanceId]);
      }, options);

      return;
    },
//-------------------------------------------------------------------------------------------------------------------------------------	
	insertrgraphChart: function(rgraphChart, viewMode, formattedRGraph, options, wysiwygInstance) {

      if(typeof Drupal.settings.tagmap == 'undefined') {
        Drupal.settings.tagmap = { };
      }
      var imgElement = $(this.stripDivs(formattedRGraph));
      this.addImageAttributes(imgElement, rgraphChart.fid, viewMode, options);

      var toInsert = this.outerHTML(imgElement);
      // Create an inline tag
      var inlineTag = this.createTag(imgElement);
      // Add it to the tag map in case the user switches input formats
      Drupal.settings.tagmap[inlineTag] = toInsert;
      wysiwygInstance.insert(toInsert);
    },
//-------------------------------------------------------------------------------------------------------------------------------------	
	outerHTML: function(element) {
      return $('<div>').append( element.eq(0).clone() ).html();
    },
//-------------------------------------------------------------------------------------------------------------------------------------
    addImageAttributes: function(imgElement, fid, view_mode, additional) {
      imgElement.attr('fid', fid);
      imgElement.attr('view_mode', view_mode);
      if (additional) {
        for (k in additional) {
          imgElement.attr(k, additional[k]);
        }
      }
      // Class so we can find this image later.
      imgElement.addClass('rgraph-image');
    },
//-------------------------------------------------------------------------------------------------------------------------------------
	stripDivs: function(formattedRGraph) {
      return $('<div>').append( $('img', $(formattedRGraph)).eq(0).clone() ).html();
    },
//-------------------------------------------------------------------------------------------------------------------------------------	
	createTag: function(imgNode) {
      
      var rgraphAttributes = {};

      var imgElement = imgNode[0];

      for (i=0; i< imgElement.attributes.length; i++) {
        var attr = imgElement.attributes[i];
        if (attr.specified == true) {
          rgraphAttributes[attr.name] = attr.value;
        }
      }
	  
      tagContent = {
        "type": 'rgraph',
        "view_mode": imgNode.attr('view_mode'),
        "fid" : imgNode.attr('fid'),
        "attributes": rgraphAttributes
      };
      return '[[' + JSON.stringify(tagContent) + ']]';
    },
//-------------------------------------------------------------------------------------------------------------------------------------	
	attach: function(content, settings, instanceId) {
      var matches = content.match(/\[\[.*?\]\]/g);
      var tagmap = Drupal.settings.tagmap;
      if(matches) {
        var inlineTag = "";
        for (i = 0; i < matches.length; i++) {
          inlineTag = matches[i];
          if (tagmap[inlineTag]) {
            // This probably needs some work...
            // We need to somehow get the fid propogated here.
            // We really want to
            var tagContent = tagmap[inlineTag];
            var rgraphMarkup = this.stripDivs(tagContent); // THis is <div>..<img>

            var _tag = inlineTag;
            _tag = _tag.replace('[[','');
            _tag = _tag.replace(']]','');
            rgraphObj = JSON.parse(_tag);

            var imgElement = $(rgraphMarkup);
            this.addImageAttributes(imgElement, rgraphObj.fid, rgraphObj.view_mode);
            var toInsert = this.outerHTML(imgElement);
            content = content.replace(inlineTag, toInsert);
          } else {
            
          }
        }
      }
      return content;
    },
//-------------------------------------------------------------------------------------------------------------------------------------
	detach: function(content, settings, instanceId) {
      var content = $('<div>' + content + '</div>');
      $('img.rgraph-image',content).each(function (elem) {
        tagContent = this.createTag($(this));
        $(this).replaceWith(tagContent);
      });
      return content.html();
    },
//-------------------------------------------------------------------------------------------------------------------------------------	
	getDefaults: function() {
		return {
			global: {
			  types: [], // Types to allow, defaults to all.
			  activePlugins: [] // If provided, a list of plugins which should be enabled.
			},
			widget: { // Settings for the actual iFrame which is launched.
			  src: Drupal.settings.rgraph.browserUrl, // Src of the rgraph browser (if you want to totally override it)
			  onLoad: this.rgraphBrowserOnLoad // Onload function when iFrame loads.
			},
			dialog: this.getDialogOptions()
	  };
	},
//-------------------------------------------------------------------------------------------------------------------------------------	
	rgraphBrowserOnLoad: function (e) {
	  var options = e.data;
	  if (this.selectedRGraph.length > 0) {
		var ok = $(this).dialog('option', 'buttons')['OK'];
		ok.call(this);
		return;
	  }
	},
//-------------------------------------------------------------------------------------------------------------------------------------	
	getDialogOptions: function() {
	  return {
		buttons: {},
		modal: true,
		draggable: true,
		resizable: true,
		minWidth: 600,
		width: 1000,
		height:700,
		position: 'top',
		overlay: {
		  backgroundColor: '#000000',
		  opacity: 0.4
		}
	  };
	},
//-------------------------------------------------------------------------------------------------------------------------------------	
	setDialogPadding: function(dialogElement) {
	  var horizontalPadding = 30;
	  var verticalPadding = 30;

	  dialogElement.width(dialogElement.dialog('option', 'width') - horizontalPadding)
	  dialogElement.height(dialogElement.dialog('option', 'height') - verticalPadding);
	},
//-------------------------------------------------------------------------------------------------------------------------------------	
	getPopupIframe: function(src, id, options) {
	  var defaults = {width: '800px', scrolling: 'no'};
	  var options = $.extend({}, defaults, options);

	  return $('<iframe class="rgraph-modal-frame"/>')
	  .attr('src', src)
	  .attr('width', options.width)
	  .attr('id', id)
	  .attr('scrolling', options.scrolling)
	},
//-------------------------------------------------------------------------------------------------------------------------------------	
	rgraphStyleSelector: function(rgraphFile, onSelect, options) {
	  var defaults = this.getDefaults();
	  defaults.src = defaults.src.replace('-rgraph_id-', rgraphFile.fid);
	  options = $.extend({}, defaults, options);
	  // Create it as a modal window.
	  var rgraphIframe = this.getPopupIframe(options.src, 'rgraphStyleSelector');
	  // Attach the onLoad event
	  rgraphIframe.bind('load', options, options.onLoad);

	  /**
	   * Set up the button text
	   */
	  var ok = 'OK';
	  var cancel = 'Cancel';
	  var notSelected = 'Very sorry, there was an unknown error embedding rgraph.';

	  if (Drupal && Drupal.t) {
		ok = Drupal.t(ok);
		cancel = Drupal.t(cancel);
		notSelected = Drupal.t(notSelected);
	  }

	  // @todo: let some options come through here. Currently can't be changed.
	  var dialogOptions = this.getDialogOptions();

	  dialogOptions.buttons[ok] = function () {

		var formattedrgraph = this.contentWindow.Drupal.rgraph.formatForm.getFormattedrgraph();
		if (!formattedrgraph) {
		  alert(notSelected);
		  return;
		}
		onSelect(formattedrgraph);
		$(this).dialog("close");
	  };

	  dialogOptions.buttons[cancel] = function () {
		$(this).dialog("close");
	  };

	  this.setDialogPadding(rgraphIframe.dialog(dialogOptions));
	  // Remove the title bar.
	  rgraphIframe.parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
	  return rgraphIframe;
	},
	rgraphBrowserOnLoad: function(e) {
	},
//-------------------------------------------------------------------------------------------------------------------------------------	
	launchBrowserWindow: function(onSelect, globalOptions, pluginOptions, widgetOptions) {
	  
	  var options = this.getDefaults();
	  options.global = $.extend({}, options.global, globalOptions);
	  options.plugins = pluginOptions;
	  options.widget = $.extend({}, options.widget, widgetOptions);
	  
	  // Create it as a modal window.
	  var browserSrc = options.widget.src;
	  // Params to send along to the iframe.  WIP.
	  var params = {};
	  $.extend(params, options.global);
	  params.plugins = options.plugins

	  // browserSrc += '&' + $.param(params);
	  var rgraphIframe = this.getPopupIframe(browserSrc, 'rgraphBrowser');
	  // Attach the onLoad event
	  // alert (rgraphIframe);
	  rgraphIframe.bind('load', options, options.widget.onLoad);
	  
	  var ok = 'OK';
	  var cancel = 'Cancel';
	  var notSelected = 'You have not selected anything!';

	  if (Drupal && Drupal.t) {
		ok = Drupal.t(ok);
		cancel = Drupal.t(cancel);
		notSelected = Drupal.t(notSelected);
	  }

	  var dialogOptions = options.dialog;

	  dialogOptions.buttons[ok] = function () {
		var selected = this.selectedRGraph;
		if (selected.length < 1) {
		  alert(notSelected);
		  return;
		}
		onSelect(selected);
		$(this).dialog("close");
	  };

	  dialogOptions.buttons[cancel] = function () {
		$(this).dialog("close");
	  };

	  this.setDialogPadding(rgraphIframe.dialog(dialogOptions));
	  return rgraphIframe;
	},
};
})(jQuery);