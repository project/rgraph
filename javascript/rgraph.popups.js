
(function ($) {

namespace('Drupal.rgraph.popups');

/**
 * RGraph browser popup. Creates a rgraph browser dialog.
 *
 * 
 */
Drupal.rgraph.popups.rgraphBrowser = function(onSelect, globalOptions, pluginOptions, widgetOptions) {
  var options = Drupal.rgraph.popups.rgraphBrowser.getDefaults();
  options.global = $.extend({}, options.global, globalOptions);
  options.plugins = pluginOptions;
  options.widget = $.extend({}, options.widget, widgetOptions);
  
  // Create it as a modal window.
  var browserSrc = options.widget.src;
  // Params to send along to the iframe.  WIP.
  var params = {};
  $.extend(params, options.global);
  params.plugins = options.plugins

  browserSrc += '&' + $.param(params);
  var rgraphIframe = Drupal.rgraph.popups.getPopupIframe(browserSrc, 'rgraphBrowser');
  // Attach the onLoad event
  rgraphIframe.bind('load', options, options.widget.onLoad);
  /**
   * Setting up the modal dialog
   */
  
  var ok = 'OK';
  var cancel = 'Cancel';
  var notSelected = 'You have not selected anything!';

  if (Drupal && Drupal.t) {
    ok = Drupal.t(ok);
    cancel = Drupal.t(cancel);
    notSelected = Drupal.t(notSelected);
  }

  // @todo: let some options come through here. Currently can't be changed.
  var dialogOptions = options.dialog;

  dialogOptions.buttons[ok] = function () {
    var selected = this.contentWindow.Drupal.rgraph.browser.selectedRGraph;
    if (selected.length < 1) {
      alert(notSelected);
      return;
    }
    onSelect(selected);
    $(this).dialog("close");
  };

  dialogOptions.buttons[cancel] = function () {
    $(this).dialog("close");
  };

  Drupal.rgraph.popups.setDialogPadding(rgraphIframe.dialog(dialogOptions));
  // Remove the title bar.
  //rgraphIframe.parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
  return rgraphIframe;
};

Drupal.rgraph.popups.rgraphBrowser.rgraphBrowserOnLoad = function (e) {
  var options = e.data;
  if (this.contentWindow.Drupal.rgraph.browser.selectedRGraph.length > 0) {
    var ok = $(this).dialog('option', 'buttons')['OK'];
    ok.call(this);
    return;
  }
};

Drupal.rgraph.popups.rgraphBrowser.getDefaults = function() {
  return {
    global: {
      types: [], // Types to allow, defaults to all.
      activePlugins: [] // If provided, a list of plugins which should be enabled.
    },
    widget: { // Settings for the actual iFrame which is launched.
      src: Drupal.settings.rgraph.browserUrl, // Src of the rgraph browser (if you want to totally override it)
      onLoad: Drupal.rgraph.popups.rgraphBrowser.rgraphBrowserOnLoad // Onload function when iFrame loads.
    },
    dialog: Drupal.rgraph.popups.getDialogOptions()
  };
}

/**
 * Style chooser Popup. Creates a dialog for a user to choose a rgraph style.
 *
 */
Drupal.rgraph.popups.rgraphStyleSelector = function(rgraphFile, onSelect, options) {
  var defaults = Drupal.rgraph.popups.rgraphStyleSelector.getDefaults();
  // @todo: remove this awful hack :(
  defaults.src = defaults.src.replace('-rgraph_id-', rgraphFile.fid);
  options = $.extend({}, defaults, options);
  // Create it as a modal window.
  var rgraphIframe = Drupal.rgraph.popups.getPopupIframe(options.src, 'rgraphStyleSelector');
  // Attach the onLoad event
  rgraphIframe.bind('load', options, options.onLoad);

  /**
   * Set up the button text
   */
  var ok = 'OK';
  var cancel = 'Cancel';
  var notSelected = 'Very sorry, there was an unknown error embedding rgraph.';

  if (Drupal && Drupal.t) {
    ok = Drupal.t(ok);
    cancel = Drupal.t(cancel);
    notSelected = Drupal.t(notSelected);
  }

  // @todo: let some options come through here. Currently can't be changed.
  var dialogOptions = Drupal.rgraph.popups.getDialogOptions();

  dialogOptions.buttons[ok] = function () {

    var formattedRGraph = this.contentWindow.Drupal.rgraph.formatForm.getFormattedRGraph();
    if (!formattedRGraph) {
      alert(notSelected);
      return;
    }
    onSelect(formattedRGraph);
    $(this).dialog("close");
  };

  dialogOptions.buttons[cancel] = function () {
    $(this).dialog("close");
  };

  Drupal.rgraph.popups.setDialogPadding(rgraphIframe.dialog(dialogOptions));
  // Remove the title bar.
  rgraphIframe.parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
  return rgraphIframe;
}

Drupal.rgraph.popups.rgraphStyleSelector.rgraphBrowserOnLoad = function(e) {
}

Drupal.rgraph.popups.rgraphStyleSelector.getDefaults = function() {
  return {
    src: Drupal.settings.rgraph.styleSelectorUrl,
    onLoad: Drupal.rgraph.popups.rgraphStyleSelector.rgraphBrowserOnLoad
  };
}


/**
 * Generic functions to both the rgraph-browser and style selector
 */

/**
 * Returns the commonly used options for the dialog.
 */
Drupal.rgraph.popups.getDialogOptions = function() {
  return {
    buttons: {},
    modal: true,
    draggable: true,
    resizable: true,
    minWidth: 600,
    width: 800,
    height:500,
    position: 'top',
    overlay: {
      backgroundColor: '#000000',
      opacity: 0.4
    }
  };
};

/**
 * Created padding on a dialog
 *
 * @param jQuery dialogElement
 *  The element which has .dialog() attached to it.
 */
Drupal.rgraph.popups.setDialogPadding = function(dialogElement) {
  // @TODO: Perhaps remove this hardcoded reference
  var horizontalPadding = 30;
  var verticalPadding = 30;

  dialogElement.width(dialogElement.dialog('option', 'width') - horizontalPadding)
  dialogElement.height(dialogElement.dialog('option', 'height') - verticalPadding);
};

/**
 * Get an iframe to serve as the dialog's contents. Common to both plugins.
 */
Drupal.rgraph.popups.getPopupIframe = function(src, id, options) {
  var defaults = {width: '800px', scrolling: 'no'};
  var options = $.extend({}, defaults, options);

  return $('<iframe class="rgraph-modal-frame"/>')
  .attr('src', src)
  .attr('width', options.width)
  .attr('id', id)
  .attr('scrolling', options.scrolling)
};

// end of closure
})(jQuery);
