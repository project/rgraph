/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace('Drupal.rgraph.browser');
Drupal.rgraph.browser.selectedRGraph = [];
Drupal.rgraph.browser.rgraphAdded = function(){};
Drupal.rgraph.browser.selectionFinalized = function(selectedRGraph) {
  // This is intended to be overridden if a callee wants to be triggered
  // when the rgraph selection is finalized from inside the browser.
  // This is used for the file upload form for instance.
};

(function ($) {
  Drupal.behaviors.experimentalRGraphBrowser = {
    attach: function (context) {
      if (Drupal.settings.rgraph.selectedRGraph) {
        Drupal.rgraph.browser.selectRGraph(Drupal.settings.rgraph.selectedRGraph);
        // Fire a confirmation of some sort.
        Drupal.rgraph.browser.finalizeSelection();
      }
      $('#rgraph-browser-tabset').tabs();
    }
    // Wait for additional params to be passed in.
  }

  Drupal.rgraph.browser.launch = function() {

  }
  Drupal.rgraph.browser.selectRGraph = function(selectedRGraph) {
    Drupal.rgraph.browser.selectedRGraph = selectedRGraph;
  }

  Drupal.rgraph.browser.finalizeSelection = function() {
    if (!Drupal.rgraph.browser.selectedRGraph) {
      throw new exception('Cannot continue, nothing selected');
    } else {
      Drupal.rgraph.browser.selectionFinalized(Drupal.rgraph.browser.selectedRGraph);
    }
  }

}(jQuery));