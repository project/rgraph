<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

class views_rgraph_plugin_style extends views_plugin_style_table {
  function format_options(&$format_options) {
    parent::format_options($format_options);
  	$format_options['char_type'] = '';
  	$format_options['char_type']['format_options'] = 'bar';
  	
  	$format_options['char_type_bar']='';
  	$format_options['char_type_bar']['barMargin'] =  '1';
  	$format_options['char_type_bar']['barGroupMargin'] = '10';
  	
  	$format_options['char_type_line_area']='';
  	$format_options['char_type_line_area']['lineWeight'] = '4';
  	
  	$format_options['char_type_pie']='';
  	$format_options['char_type_pie']['pieMargin']='20';
  	$format_options['char_type_pie']['pieLabelPos']=array();
  	
    $format_options['global'] = '';
    $format_options['global']['hide_table'] ;
  	$format_options['global']['width'] = '';
  	$format_options['global']['height'] = '';
  	$format_options['global']['appendTitle'] = array();
  	$format_options['global']['title'] = '';
  	$format_options['global']['appendKey'] = array();
  	$format_options['global']['colors'] = '#be1e2d,#666699,#92d5ea,#ee8310,#8d10ee,#5a3b16,#26a4ed,#f45a90,#e9e744';
  	$format_options['global']['textColors'] = '';
  	$format_options['global']['parseDirection'] = array();
  }

  function format_options_form(&$form, &$form_values) {
    parent::format_options_form($form, $form_values);
  	$form['char_type']= array(
  	  '#type' => 'fieldset',
  	  '#title' => t('Chart Type'),
  	  '#collapsible' => FALSE,
  	  '#collapsed' => FALSE,
  	);
  	$form['char_type']['format_options'] = array(
  	  '#type' => 'radios',
  	  '#title' => t('Options'),
  	  '#description' => t('Graph type'),
  	  '#default_value' => $this->format_options['char_type']['format_options'],
  	  '#required' => TRUE,
  	  '#format_options' => array('bar' => t('Bar'), 'line' => t('Line'), 'area' => t('Area'), 'pie' => t('Pie')),
  	);
  	$form['global']= array(
  	  '#type' => 'fieldset',
  	  '#title' => t('Options'),
  	  '#collapsible' => FALSE,
  	  '#collapsed' => FALSE,
  	);
    $form['global']['hide_table'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show only the graph.'),
    '#description' => t('Hides the table'),
    '#default_value' => $this->format_options['global']['hide_table'] ,
    );
  	$form['global']['width'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('width'),
  	  '#description' => t('Graph width, defaults to table width'),
  	  '#size' => 5,
  	  '#maxlength' => 5,
  	  '#default_value' => $this->format_options['global']['width'] ,
  	);
  	$form['global']['height'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('height'),
  	  '#description' => t('Graph height, defaults to table height'),
  	  '#size' => 5,
  	  '#maxlength' => 5,
  	  '#default_value' => $this->format_options['global']['height'] ,
  	);
   	$form['global']['appendTitle'] = array(
  	  '#type' => 'select',
  	  '#title' => t('appendTitle'),
   	  '#description' => t('Show a title in the graph. "title" field value will be used'),
  	  '#format_options' => array(
  		True => t('Yes'),
  		False => t('No'),
  	  ),
  	  '#default_value' => $this->format_options['global']['appendTitle'] ,
  	);
  	$form['global']['title'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('title'),
  	  '#size' => 60,
  	  '#maxlength' => 128,
  	  '#default_value' => $this->format_options['global']['title'] ,
  	);
   	$form['global']['appendKey'] = array(
  	  '#type' => 'select',
  	  '#title' => t('appendKey'),
   	  '#description' => t('Add the color key to the chart.'),
  	  '#format_options' => array(
    		True => t('Yes'),
    		False => t('No'),
  	  ),
  	  '#default_value' => $this->format_options['global']['appendKey'] ,
  	);
  	$form['global']['colors'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('colors'),
  	  '#description' => t('Items are hex values, used in order of appearance.'),
  	  '#size' => 60,
  	  '#default_value' => $this->format_options['global']['colors'] ,
  	);	
  	$form['global']['textColors'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('textColors'),
  	  '#description' => t('Items are hex values. Each item corresponds with colors array'),
  	  '#size' => 60,
  	  '#default_value' => $this->format_options['global']['textColors'] ,
  	);
   	$form['global']['parseDirection'] = array(
  	  '#type' => 'select',
  	  '#title' => t('parseDirection'),
   	  '#description' => t('Direction to parse the table data'),
  	  '#format_options' => array(
    		'x' => t('x'),
    		'y' => t('y'),
  	  ),
  	  '#default_value' => $this->format_options['global']['parseDirection'] ,
  	);
  
     $form['char_type_bar']= array(
  	  '#type' => 'fieldset',
  	  '#title' => t('Bar Chart Type option'),
  	  '#collapsible' => FALSE,
  	  '#collapsed' => FALSE,
  	);	
  	
  		$form['char_type_bar']['barMargin'] = array(
  		  '#type' => 'textfield',
  		  '#title' => t('barMargin'),
  		  '#description' => t('Creates space around bars in bar chart (added to both sides of each bar).'),
  		  '#size' => 5,
  		  '#maxlength' => 5,
  		  '#default_value' => $this->format_options['char_type_bar']['barMargin'],
  		);
  		$form['char_type_bar']['barGroupMargin'] = array(
  		  '#type' => 'textfield',
  		  '#title' => t('barGroupMargin'),
  		  '#description' => t('Space around each group of bars in a bar chart.'),
  		  '#size' => 5,
  		  '#maxlength' => 5,
  		  '#default_value' => $this->format_options['char_type_bar']['barMargin'],
  		);
     
     $form['char_type_line_area']= array(
  	  '#type' => 'fieldset',
  	  '#title' => t('Line and Area Chart Type option'),
  	  '#collapsible' => FALSE,
  	  '#collapsed' => FALSE,
  	);	
  		$form['char_type_line_area']['lineWeight'] = array(
  		  '#type' => 'textfield',
  		  '#title' => t('lineWeight'),
  		  '#description' => t('Stroke weight for lines in line and area charts.'),
  		  '#size' => 5,
  		  '#maxlength' => 5,
  		  '#default_value' => $this->format_options['char_type_line_area']['lineWeight'],
  		);	
     $form['char_type_pie']= array(
  	  '#type' => 'fieldset',
  	  '#title' => t('Pie Chart Type option'),
  	  '#collapsible' => FALSE,
  	  '#collapsed' => FALSE,
  	);
  
  		$form['char_type_pie']['pieMargin'] = array(
  		  '#type' => 'textfield',
  		  '#title' => t('pieMargin'),
  		  '#description' => t('Space around outer circle of Pie chart.'),
  		  '#size' => 5,
  		  '#maxlength' => 5,
  		  '#default_value' => $this->format_options['char_type_pie']['pieMargin'],
  		);		
  		$form['char_type_pie']['pieLabelPos'] = array(
  		  '#type' => 'select',
  		  '#title' => t('pieLabelPos'),
  		  '#description' => t("Position of text labels in Pie chart. Accepts 'inside' and 'outside'."),
  		  '#format_options' => array(
  			  'inside' => t('inside'),
  			  'outside' => t('outside'),
  		  ),
  		  '#default_value' => $this->format_options['char_type_pie']['pieLabelPos'],
  		);   
  }

  function format_options_validate(&$form, &$form_values) {
  }

}

