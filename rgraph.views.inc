<?php
// $Revision$
// $Date$
// $Log$
// Revision 1.3  2010/08/17 12:57:36  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 11:58:33  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//
// Revision 1.2  2010/08/15 10:40:01  maulikkamdar
// Initial commit of rgraph module. Creates specialized canvas graphs using a requirement of extended_html module.
//

/**
* Implementation of hook_views_plugins
*
*
*/
function rgraph_views_plugins() {

  $plugins = array();
  
  $plugins['style'] = array('rgraph' => array(
        'title' => t('RGraph'),
        'help' => t('Allows to use the Rgraph plugin as a View style plugin'),
        'handler' => 'views_rgraph_plugin_style',
        'path' => drupal_get_path('module', 'rgraph'),        
		'parent' => 'table',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,        
        'type' => 'normal',
		'theme' => 'views_view_rgraph',
      ),
  );

  return $plugins;
}
